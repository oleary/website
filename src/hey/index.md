## What I do

I'm currently a [Developer Evanglist](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/) at [GitLab](https://about.gitlab.com/).

## What I've done

- [Resume](/resume/)
- [Talks](/talks/)
- [Portfolio](/portfolio/)
- [Side Projects](/side-projects/)

## Get in Touch

- [Twitter](https://twitter.com/olearycrew)
- Email: `boleary` `[at]` `hey.com`
- [LinkedIn](https://www.linkedin.com/in/olearycrew/)
