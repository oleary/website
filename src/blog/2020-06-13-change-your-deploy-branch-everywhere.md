---
title:  "Changing your default branch - all the places"
date:   2020-06-13 05:00:00
# tags: 
#   - gitlab
#   - issues
# image: /img/blog/homereno03.png
author: brendan
type: post
blog: true
excerpt: > 
  We shouldn't use master as a branch name anymore - here's how to change your default branch wherever you deploy your project.
meta:
  - name: "twitter:title"
    content: "Changing your default branch - all the places"
  - name: "twitter:description"
    content: "We shouldn't use master as a branch name anymore - here's how to change your default branch wherever you deploy your project."
  - name: "og:title"
    content: "Changing your default branch - all the places"
  - name: "og:description"
    content: "We shouldn't use master as a branch name anymore - here's how to change your default branch wherever you deploy your project."
---

> You can also read my blog post on [changing the default branch stream upstream in git](/blog/2020-06-10-i-was-wrong-about-git-master.html) or [renaming your own default branch in GitLab](/blog/2020-06-11-change-your-default-branch.html).

This post is a living post where I document all the different places where one may need to update the default branch when [changing the name](/blog/2020-06-11-change-your-default-branch.html) of that branch.

## Contents 
* [GitHub](#github)
* [GitLab](#gitlab)
* [Netlify](#netlify)
* [GitLab CI/CD](#gitlab-ci-cd)

## All the Places

### GitHub
[Scott Hanselman](https://twitter.com/shanselman) wrote a great post about [renaming your branch and updating GitHub](https://www.hanselman.com/blog/EasilyRenameYourGitDefaultBranchFromMasterToMain.aspx).

### GitLab
See my [previous post](/blog/2020-06-11-change-your-default-branch.html) on updating GitLab.

### Netlify
If you deploy to Netlify using their standard git connection, you'll need to tell Netlify you've updated the default branch (what they call the `production branch`).

1. Log in and find the site you just updated
1. Go to `Deploys`
1. Click `Deploy Settings`
1. Under `Deploy contexts` click `Edit settings`
1. Change the name of the production branch and click `Save`

> Note: There may still be a "branch deploy" for master after you click save.  You can remove this by just editing the settings one more time and removing `master` from the "Let me add individual branches" section.

### GitLab CI/CD
It's possible if you use GitLab CI/CD, then a simple find/replace of `master` in your `.gitlab-ci.yml` will be sufficient.   However, you could make your builds future proof by using [environmental variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) that come built-in.

For example, you may have code that says

#### Old Code
```yaml
only:
  refs:
    - master
```

Here, we could simply update `master` to the new name, but if we also look at the updated [`rules:`](https://docs.gitlab.com/ee/ci/yaml/#rules-clauses) functionality, we could refactor this to something like:

#### New Code
```yaml
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

This allows us to use the variables GitLab provides to explicitly say, "run this job only on the default branch."  Then, our CI configuration will respect any future changes to the default branch.