---
title: 'Michael Collins vs. the Billionaires'
date: 2021-07-21 02:00:00
tags: []
type: 'post'
blog: true
author: Brendan
excerpt: 'What lessons from the first space race - between Russia and the U.S. - can we apply to our view of the new space race - between billionaires.'
meta:
  - name: 'twitter:title'
    content: 'Michael Collins vs. the Billionaires'
  - name: 'twitter:description'
    content: 'What lessons from the first space race - between Russia and the U.S. - can we apply to our view of the new space race - between billionaires.'
  - name: 'og:title'
    content: 'Michael Collins vs. the Billionaires'
  - name: 'og:description'
    content: 'What lessons from the first space race - between Russia and the U.S. - can we apply to our view of the new space race - between billionaires.'
---

I've been trying to read a lot more [books](/books/) ever since the pandemic started. Massive shout out to the Libby App and the Anne Arundel County Public library because I've actually _listened_ to more books than I've read...but I'll count them anyway.

The most recent book that I've been ~~reading~~ listening to is Michael Collin's book [*Carrying the Fire*](https://amzn.to/3x6eMZP).  Michael Collins is the oft-forgotten third astronaut on the historic Apollo 11 flight to the moon. Collins kept the command module Columbia flying while Neil Armstrong and Buzz Aldrin took the Eagle lunar landing module to the moon first to step foot on it.  The book is fantastic - written in the 1970s when the flights were still fresh in Collin's mind.  I'd had it on my "to read" list for some time but hadn't gotten around to putting it on the top of the list until Collins passed away earlier this year (in April of 2021).  That reignited (get it?) my interest in *Carrying the Fire*. It just so happened that I was finishing it up through the past month that saw a renewed interest in space flight. 

With Jeff Bezos and Sir Richard Branson going to space (or the edge of space depending on who you ask), the world was once again looking up to space.  Again was sparked a curiosity that has waxed and waned over the years since the "space race" of the 1960s.  Space has always fascinated me - [Apollo 13](https://amzn.to/2UA7WOP) was one of my favorite movies as a kid. In 2019, I built a [whole talk](https://www.youtube.com/watch?v=cRGjw04ZA4M) around the Apollo Guidance Computer and the accomplishments of the great Margaret Hamilton.  I'm by no means the first person to draw both comparison and contrast with the days of NASA and the current date private space race. However, I do think that reading Collins' book during this time has given me a particular perspective.

## First, the original "Space Race"
The 1960s - a time that is oft relived in popular culture (I'm looking at you [Mad Men](https://en.wikipedia.org/wiki/Mad_Men)).  The Cold War between two superpowers in the front of many Russian and U.S. minds.  And many in the rest of the world are impacted by their clashes.  One of the ways of romanticizing this time includes talking about the "Space Race."

A prelude and parallel track to the ever-increasing military build-up of both superpowers in this time, the space race saw both nations moving quickly to try and dominate space.  And by any measure - Russia was far ahead, putting both the first man-made satellite into orbit, as well as the first human.   In the U.S., President Kennedy and then his legacy demanded action.  A public effort at a scale not seen since saw the USA accelerate from having barely entered space with rockets in 1959 to landing on the moon just ten years later.

There is no denying the nationalism and political posturing benefits derived from this race.  But hearing about it directly from the astronauts who flew the missions (such as Collins) is different.  They describe human space exploration as one of the most world-uniting activities one can imagine.  All of the astro- and cosmonauts of this era talk about seeing the Earth from above.  Up there, there are no political boundaries visible.  One can better appreciate how small and fragile our planet is in the vastness of our solar system, galaxy, and the universe.  They all describe it as a life-changing experience.

In the book, Collins talks about actually meeting the "competing" Russian cosmonauts at one point during a press junket.  And the concerns of both teams were a shared experience between the two groups, even as their countries were engaged in divisive political conflicts.  In addition, Collins, the other Apollo 11 astronauts, and NASA were careful to be clear about how the mission was not only about the United States but about all humankind. Just a few examples of this include:

- Neil Armstrong's first words on the moon: "That's one small step for [a] man, one giant leap for mankind."
- The plaque left on the moon reads: "Here men form the planet Earth / First set foot upon the moon / July 1969, A.D.  We came in peace for all mankind."
- The map on the plaque includes the entire Earth - a circle with the Western hemisphere and a circle with the Eastern Hemisphere
- In addition, a tiny silicon disk (about 1 1/2 inches in diameter) was left, which contained miniaturized messages from 73 different heads of state.

In addition, NASA at one point considered leaving a flag from each U.N. member nation...however, this is where the international flavor lost out. It was only the American flag that would fly at the landing site.  Despite that, the greater meaning of the journey - greater than one person or even one country - was clearly apparent and recognized by those closest to the mission.  And it was one watched around the world, with an estimated 1/3 of the entire world's population watching the landing live.

Debates about how international or national the effort was aside, it was - at the very least - clearly an effort bigger than the astronauts themselves.  On the 8th day of their mission, after the moon landing and just one day before they would return to Earth, the three astronauts broadcast messages from the command module.  Buzz Aldrin went second and laid it out like this:

> As we've been discussing the events that have taken place in the past two or three days here onboard our spacecraft, we've come to the conclusion that this has been far more than three men on a voyage to the moon; more, still, than the efforts of a government and industry team; more, even, than the efforts of one nation.  We feel that this stands as a symbol of the insatiable curiosity of all mankind to explore the unknown.

## Second, the spaceship measuring contest
One can't help but constrast those gracious words, words that will indeed be studied and read by generations and generations in history books, with the words and actions of some of the newest astronauts - billionaires.

First, let me state that I agree with the conclusion many have come to - that there isn't enough public interest in space travel to make public funding of a meaningful space program to continue at Apollo or even space Shuttle levels forever.  And I don't want some political struggle - like another cold war - to change that.  We need to make space travel more financially viable, and one of the ways to do that is through public-private partnerships like we've seen with NASA and Space X.

But the last few weeks have shown us a very different view of space and programs to get there.  The average psyche - American or otherwise - has shifted from "us" to "us vs. them" or "me" over the 50 years since the Apollo 11 landing.  Through the monetizing and flattening of our social interactions, we've come to view the world and our place in it much differently now.  Gone is the global view - the one without borders.  If anything, it is quite the opposite.  Now we seek to insulate ourselves not only from those outside our borders.  We even divide ourselves from those in the same country or city or town as us if they dare have different opinions than our, obviously correct, ones.

It's easy to see this in the actions of Branson and Bezos.  The definition of things like "Karman line" and what record they can set themselves, using their own personal wealth, putting their own stamp on space have all become more important than the perspective gained there.  It isn't about "us" - it's about *them*.  One needs to look no further than the discussions they've had after returning to see this.  Bezos went so far as to say, in answer to the question, "What was it like, was it everything you imagined.":

> The most fun vehicle...I also want to thank every Amazon employee and customer because you guys paid for all of this.

## Third, a decision for all of us
The easy route to take here is to criticize Bezos - and that criticism is not without merit.  But I think in some ways, this second space race and the comments around it say more about us as humans than it does about Jeffery Bezos.  I would call that more of a symptom than the root cause.

We have a choice to make - do we continue to divide ourselves into smaller and smaller groups?  Groups that are absolutely sure of their right-ness - mainly because of the "ovbious" wrongness of every other group.  Or can we rekindle the group efforts of the Apollo missions - giving glory not just to Niel, Buzz, Michael but to all who helped accomplish their task?  Or better yet, all of those who share in the common good of and work to make the Union they live in more perfect.

Or can we do one better?  Can we all ask for and receive the kind of perspective on the world available, until now, a very select few.  A view of the world that places the value of political divides and borders in their rightful place - small and invisible from the scale of our one small blue dot in a sea of seemingly endless space.  I think that perspective could help many of us feel better about our place on Earth and the place our fellow humans have on it as well.

> When it is dark enough, men see the stars
> 
> — Ralph Waldo Emerson