---
title: Blog
---

<script>
    window.location.replace("https://blog.boleary.dev");
</script>

> I've moved!

My blog is now self-hosted on [Ghost](https://ghost.org/) - you can find my new blog [here](https://blog.boleary.dev) and subscribe for updates!

## Old Posts
<br/>

<BlogPostList 
  :pages="$site.pages" 
  :page-size="$site.themeConfig.pageSize" 
  :start-page="$site.themeConfig.startPage" 
/>
