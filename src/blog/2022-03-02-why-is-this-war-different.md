---
title: "Why is this war different from all other wars?"
date: 2022-03-02 01:00:00
tags: []
type: "post"
blog: true
author: Brendan
excerpt: "What about other wars? Why is Russia's invasion of Ukraine different?"
meta:
  - name: "twitter:title"
    content: "Why is this war different from all other wars?"
  - name: "twitter:description"
    content: "What about other wars? Why is Russia's invasion of Ukraine different?"
  - name: "og:title"
    content: "Why is this war different from all other wars?"
  - name: "og:description"
    content: "What about other wars? Why is Russia's invasion of Ukraine different?"
---

<blockquote class="info">
  <p>
    NOTE: Sorry for the interruption in your regularly scheduled tech content. I swear I'll write more about tech when I can.
  </p>
</blockquote>

Okay, I don't pretend to know everything, but I have seen a lot of ["whataboutism"](https://www.merriam-webster.com/words-at-play/whataboutism-origin-meaning) around the Russian invasion of Ukraine and "why are we treating this differently than X"

Now - all war is terrible...full stop. But I do think several things make this unique:

1. **Sovereignty**. There is no question in the international community over the sovereignty of Ukraine and its borders. No unsettled debate; Russia directly invaded a sovereign nation.
2. **Unprovoked**. It was utterly unprovoked; only later did Russia try to come up with reasons for the aggression.
3. **Lack of Public Support**. The reasons Russia does list do not hold water with anyone in the international community. And they don't match with the public sentiment in Ukraine. I challenge someone to find me FIVE people in all of Kyiv (pop. 2.8 million) who support the aggression
4. **No existing conflict**. To that point, there was no current conflict in Ukraine. There is no civil war, no break-off factions in any regions that Russia is bombing. In fact, the Ukrainian people did recently go through this, choosing democracy, choosing the EU, choosing the West through public protest and support.
5. **Democracy**. Ukraine has a fully functioning democracy. They had elected the government that is in place now. There was no dictator at the helm threatening the general public. Look no further than the massive public support for Zelensky - including from other parties - to prove that.
6. **Premeditated**. This invasion wasn't the result of some massive change in the situation on the ground. Putin has been planning this for a long time - probably since the Ukrainian revolution. He's doing it to suit his needs, not the needs or desires of any of the people he's invading.
7. **Superfluous**. Because Putin miscalculated the world, he has endangered Russia more than ever. Even the Soviet Union had more international support than the Russian government has today. Thus, any argument about Russian safety from NATO is null and void - it was about Putin and his power.
8. **War in Europe**. Many are tempted to say it's only covered because it's in Europe. Well, let's say that's true - it is because that is where World Wars have started. With all the wars we've seen since 1900, the only two to go global were ones on the [European Plain](https://en.wikipedia.org/wiki/European_Plain) because of its strategic importance. So it is a big deal.
9. **Rejected diplomacy**. In many cases before, where a larger nation thought they had cause to invade a smaller one, they have first offered diplomatic solutions. And when the smaller country refused, only then did they invade. This conflict flips it on its head. The smaller nation begged for diplomacy, and Russia rejected it.
10. **Nuclear Option**. In other wars of aggression from large, nuclear-capable nations, we did not doubt that nation would restrain itself from the unthinkable. Just mere days into this war, Putin raised the specter of nuclear weapons against anyone who would dare question him.

Even if you disagree with me on points 1-9, number 10 presents a clear and present danger unlike any we've seen since WWII and the Cold War. If no other reason, that is why Putin must be treated differently. If no other reason, that is why the entire world has a stake in this war, unlike other current and past conflicts.

<blockquote class="success">
  <p>
    For a list of vetted resources you can donate to help Ukrainians suffering from this war, <a href="https://brendan.fyi/helpukraine">click here</a>.
  </p>
</blockquote>