---
title: 'New Year, New Language'
date: 2022-01-10 01:00:00
tags: []
type: 'post'
blog: true
author: Brendan
excerpt: 'I want to learn a new programming language...in public'
meta:
  - name: 'twitter:title'
    content: 'New Year, New Language'
  - name: 'twitter:description'
    content: 'I want to learn a new programming language...in public'
  - name: 'og:title'
    content: 'New Year, New Language'
  - name: 'og:description'
    content: 'I want to learn a new programming language...in public'
---

Well, it's been a little while since I've written in this space. Who knows why that could be - say maybe it was the holiday time? Or a busy time at work? Or perhaps it's just the ongoing pandemic that is great at taking away the motivation to be productive in any way in any aspect of life.

Whatever the reason, I'd love to change the fact that it's been months since I've written anything. To do that, I've combined a few things that I've wanted to do for a while into one "mega" project. And I'll be documenting the progress of that megaproject here - so if you're new here, then welcome, and this is what we do - take wide left turns now and then.

## My 2022 Goals
What I've been thinking about doing this year are things like:
- Learning a new programming language (like Go or Rust)
- Teaching what I know more
- Contributing to GitLab and the GitLab Runner
- Helping others do the same
- Promoting the idea that "everyone can contribute" to help folks who might want to learn about programming for the first time.

When thinking about these goals at first, they seem to be very different - but I think I've found a way to combine them in a way that should be fun and educational. I'm planning on learning Go in public - probably through a combination of Twitch, produced videos, and written content. I'll do as much as I can to make the whole process open to the world - while also hopefully summarizing my learning in a valuable way to folks who come after me.

## The Project

I don't have the exact structure or timing down yet. Still, generally, I think it breaks down something like: first, learn the basics of Go (concepts, terminology, etc.). Then build a basic project with Go. And finally, apply what I understand to contribute to a more mature project like the GitLab Runner or GitLab Container Registry. That will allow me to illustrate both the concepts required and their application to real-world problems...which has always been the way I've been able to learn best.

And so, I held a [Twitter poll](https://twitter.com/olearycrew/status/1479504338494136327) to figure out what those early projects would be. It was a close race, but a Twitter bot was the winner. However, my impatience during the poll lead me to buy a domain for another answer - the Slack emoji package manager - so I think I'll build both 🙂.

## How to follow along
The exact form of the output of these projects (other than the code) is still up in the air. But I'll be for sure updating you in this space as we go. And I've also created two public GitLab issues for you to follow along as well with your favorite project - or comment your ideas!

- [Slash Bruno Twitter bot](https://gitlab.com/brendan/brendan/-/issues/39)
- [Emoji Package Manager for Slack](https://gitlab.com/brendan/brendan/-/issues/35)

I'd love to hear what you think - follow this space or [Follow me on Twitter](https://twitter.com/olearycrew) to see how this all works out!