---
title:  "My next move at GitLab"
date:   2020-03-11 05:00:00
tags: 
  - gitlab
  - meta
  - personal
#image: /img/blog/pihole1.png
type: post
blog: true
author: brendan
excerpt: > 
  I've always been a GitLab evangelist. Three years in, I'm excited to be taking a new step at GitLab and putting evangelist directly into my title 😃.
---


I'm coming up on my third year at GitLab.  Or as some people like to put it - I've been at GitLab for [1,049+ hires](https://about.gitlab.com/company/team/).  GitLab has been growing rapidly, and I've been lucky enough to be there for [three](https://about.gitlab.com/blog/2017/10/09/gitlab-raises-20-million-to-complete-devops/) [separate](https://about.gitlab.com/blog/2018/09/19/announcing-100m-series-d-funding/) [funding rounds](https://about.gitlab.com/blog/2019/09/17/gitlab-series-e-funding/).

All of that growth has been incredible to watch and be a part of.  And even before I worked at GitLab, I was a large proponent of GitLab and the concept of bringing CI and CD right next to the code.  Having worked as both an Engineering Manager and a Product Manager in the past, I've lived the pain of having a team have to spend more time fixing the tools than actually coding solutions to customer problems.

## Always an Evangelist
Given that background, and previous lives spent wrestling with various DevOps toolchains, I've always, always been an evangelist for GitLab and GitLab CI/CD. Ever since I wrote my first pipeline for my side project - [listMe, a Slack app for managing lists](https://listme.chat) I knew GitLab CI/CD was something special.

From there, I went on to evangelize GitLab in my role at the time as the director of DevOps at a small federal government contractor.  Their environments were vastly different - production was bare metal, the customer's test environment one virtualization platform, and our DEV environment yet another platform.   Bringing GitLab CI/CD to that problem allowed me to control those variables that we could, and eliminate mistakes by reducing complexity.

In late 2017, I was given the opportunity to get "on the bus" at GitLab.  After a friend was recruited to GitLab, they recommended I take a look.  "Oh...it's a company, not just a tool," I thought.  And later that month, on a rainy beach vacation, I had enough time to read and fall in love with the [GitLab handbook](https://about.gitlab.com/handbook) and [values](https://about.gitlab.com/handbook/values/).  From that day on, I was an evangelist for GitLab, the company,too.  Much to the chagrin of my wife and family that week.

## At GitLab
At GitLab, I was able to help start our professional services group, run product management for a time for the CI product I loved, and contribute in a lot of other ways.  True to our Everyone Can Contribute mission, I was able to pitch into the product itself, various functions, and help onboard lots of new team members.

That whole time, I have been fortunate enough to be able to talk, write, and tweet about GitLab, the company, and the product to anyone that would listen.  So I'm excited that now as we scale, I've been given the opportunity to become a Developer Evangelist full time.  My new boss, [Priyanka Sharma](https://twitter.com/pritianka), is building a fantastic team. We [plan to focus](https://about.gitlab.com/handbook/marketing/technical-evangelism/) on doubling down on our open source roots, evangelize DevOps and simplified tools chains and ensure that we can make GitLab as amazing for everyone as it was for me in those early days.

I can't wait to see where we'll go next, and I know that I will be a [GitLabber](https://about.gitlab.com/handbook/communication/#top-misused-terms) for the rest of my life.  For now, I'm excited that sharing my passion for DevOps tooling, #AllRemote, and the [GitLab values](https://about.gitlab.com/handbook/values/) is now officially my full-time job!

## Follow Along
If you'd like to follow what my team and I are doing - we're, of course, [public by default](https://about.gitlab.com/handbook/values/#public-by-default).  I would love it if folks took a look at my [2020 Technical Evangelism](https://gitlab.com/brendan/2020-dev-evangelism) plan, which outlines at a high level what I plan to focus on this year. You can also check out our [issue board here](https://gitlab.com/groups/gitlab-com/-/boards/1565342?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=tech-evangelism) to see all of the tactical things we have in the pipeline.  And of course, [follow me on Twitter](https://twitter.com/olearycrew) to see the day-to-day work it takes for me to realize this dream and evangelize GitLab.
