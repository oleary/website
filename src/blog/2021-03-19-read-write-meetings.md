---
title: 'Running meetings in read-write mode instead of read-only'
date: 2021-03-19 02:00:00
tags: []
type: 'post'
blog: true
author: Brendan
excerpt: 'Meetings are often the WORST.  Going remote in a pandemic makes them even more draining.  Break your team out of this pattern with this easy change.'
meta:
  - name: 'twitter:title'
    content: 'Running meetings in read-write mode instead of read-only'
  - name: 'twitter:description'
    content: 'Meetings are often the WORST.  Going remote in a pandemic makes them even more draining.  Break your team out of this pattern with this easy change.'
  - name: 'og:title'
    content: 'Running meetings in read-write mode instead of read-only'
  - name: 'og:description'
    content: 'Meetings are often the WORST.  Going remote in a pandemic makes them even more draining.  Break your team out of this pattern with this easy change.'
---

Early on in the pandemic, there was a mad rush by many to figure out how to do their jobs remotely. Collaboration, communication, and coordination, all typically done when together in person, were suddenly impossible or highly unlikely. And when you're used to being able to meet in person, "grad a conference room," or be at a whiteboard with others, this can be a very jarring experience. Many of us were not prepared as we've always been in an office and never experienced remote work before.

<div style="display: flex; align-items: center; justify-content: center;">
  <BlogImage image="https://s2.svgbox.net/illlustrations.svg?ic=blackboard" width="55%" talign="center" caption="Meetings suck.  Learn to make them better" />
</div>

While this was true across all industries and businesses, one place I got to see it up close was in education. While I've been working at home for years, for a company with [no offices at all](https://about.gitlab.com/company/culture/all-remote/), that is not the standard model for many. Not the least of who are educators. My mother, my wife, my two sisters are all educators - and I saw the struggle it can be to take what is traditionally a very much hands-on, in-person endeavor and try and recreate it virtually.

While I could - and may, don't tempt me - write an entire blog series on what I learned worked well. What didn't in remote education, I think there is a universal thing that came up relatively soon into the pandemic that has broader implications for educators and everyone still trying to understand how to work remotely with their colleagues. That subject? Well, it's everyone's favorite activity: meetings.

## Why meetings suck

Meetings are often the WORST. You don't have to look far on the internet for the universal "this meeting coul dhave been an email" memes - they are everywhere. And for a good reason. When we're in-person together in the exact physical location, and simultaneously, it can be the lazy way of getting. Things "done" to have a meeting. Want input from other people? Schedule a meeting. Want to reach a consensus on a decision? Schedule a meeting. Want to plan out the next fiscal year? You're probably going to need a few sessions to schedule the meetings you'll need.

But that's what has traditionally sucked about meetings - the fact they are low friction in an in-person world, especially for the person calling the meeting or the person with the [HiPPO](https://www.inc.com/scott-mautz/the-highest-paid-person-in-meeting-is-most-dangerous-voice-according-to-whartons-adam-grant.html) (highest paid person's opinion) to feel better about their opinion by voicing it in a public forum.

## Making bad meetings worse

Add into that problem the addition of being [Suddenly Remote](https://www.notion.so/boleary/Suddenly-Remote-80855f8206064f3d9e38f1a2563bd200) and the difficulties become compounded. When you're not used to running meetings remotely - those meetings can become worse and less valuable. For instance, it can be even more challenging for folks to participate or have their opinions heard or questions answers. Worse still, virtual meetings are more tiring than in-person ones. So if your organization just took your typical meeting schedule and put it online - you're in for a world of pain. Not to mention the inability to find a free minute to go to the bathroom.

While meetings at the school where my mom works were not back to back - she talked to me early about how they weren't working. There seemed only to be two modes: either the principal would be presenting something and "talk at" the staff for an hour OR there would be chaos as people tried to collaborate and talk over one another - and scramble to find the mute button. She knew that there had to be a better way - and that she had a son (me) who had been working remotely long before the pandemic. She called and asked if I had any ideas...and boy did I!

## Make virtual meetings not suck

At GitLab, we use something called [Live Doc meetings](https://about.gitlab.com/company/culture/all-remote/live-doc-meetings/). Every meeting has a Google doc agenda attached to the meeting invite. Every. Single. Meeting. I've been known to show up to informal "[coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats)" with a Google Doc prepared just so that we can run it like an ordinary GitLab meeting.

Having a live, multi-player document that serves as the agenda for a meeting - especially one to be conducted over Zoom - is incredibly freeing. It allows everyone in the meeting to contribute - you can combine both a formal agenda and points that the leader of the meeting wants to cover with participants' ability to contribute, ask questions and add their points.

Using simple bulleted or numbered lists is built into any document program and allows the notes and agendas to flow together. Having it be a multi-player document like Google Docs lets folks add to any point in the schedule. Having it in the calendar invite means you can easily find it before the meeting - or at meeting time. When the meeting starts, we can all take notes, add details, and link to more context _inline_ with the agenda. Want to ask a question? Just put it in the doc on the following line or after the rest of the schedule, and as the team goes down the list, eventually they'll get to the question - be sure to preface it with your name, so we know who to call on.

Establishing these kinds of norms when meeting with colleagues will help your whole team feel more productive. Instead of meetings being one-way communication that could have been an email, you'll find they are transformed into the fantastic collaboration sessions you hope they could be. You'll also be able to show more inclusion - everyone contributing to the agenda means it's not only the loudest or highest paid people who are going to be heard.

I'd love you to give this a try - and you can find a lot more details about live doc meetings [in the GitLab handbook](https://about.gitlab.com/company/culture/all-remote/live-doc-meetings/). Do these tips help? Still, have meeting fatigue? Let me know how it works by finding me on [Twitter](https://twitter.com/olearycrew).
