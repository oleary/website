const currentDateUTC = new Date().toUTCString();

module.exports = {
  title: "Brendan O'Leary",
  dest: "./public",
  themeConfig: {
    repoLabel: "Repo",
    editLinks: true,
    editLinkText: "Found a bug? Help me improve this page!",
    nav: [
      {
        emoji: "🏠",
        text: "Home",
        link: "/",
      },
      {
        emoji: "🎙️",
        text: "Talks",
        link: "/talks/",
      },
      {
        emoji: "💼",
        text: "Portfolio",
        link: "/portfolio/",
      },
      {
        emoji: "📝 ",
        text: "Blog",
        link: "https://blog.boleary.dev/",
      },
      {
        text: "More",
        items: [
          {
            text: "About Me",
            items: [
              {
                emoji: "💭",
                text: "My Work Philosophy",
                link: "/work-philosophy/",
              },
              {
                emoji: "📰",
                text: "My Resume",
                link: "/resume/",
              },
              {
                emoji: "🌃",
                text: "Side Projects",
                link: "/side-projects/",
              },
              {
                emoji: "🙊",
                text: "FAQ",
                link: "/faq/",
              },
              {
                emoji: "🗓️",
                text: "Events",
                link: "https://cfps.dev/u/brendan/events",
              },
            ],
          },
          {
            text: '"Free Thoughts"',
            items: [
              {
                emoji: "📚",
                text: "Books & Media",
                link: "https://blogs.boleary.dev/books",
              },
              {
                emoji: "🇮🇪",
                text: "Last Name",
                link: "/thoughts/name/",
              },
              {
                emoji: "⛓️",
                text: "Supply Chain Security",
                link: "/thoughts/supplychain/",
              },
              {
                emoji: "🧑‍🤝‍🧑",
                text: "Inclusion",
                link: "/thoughts/do-better/",
              },
            ],
          },
          {
            text: "The Rest",
            items: [
              {
                emoji: "🖥️",
                text: "My Desk",
                link: "/desk/",
              },
              {
                emoji: "🕸️",
                text: "My Domains",
                link: "https://boleary.dev/domains/",
              },
              {
                emoji: "🌐",
                text: "Stickers",
                link: "/stickers/",
              },
              {
                emoji: "🗄️",
                text: "Blog Archive",
                link: "/archive/",
              },
            ],
          },
        ],
      },
      {
        emoji: "🤙",
        text: "Contact",
        link: "/contact/",
      },
    ],
    logo: "/brendan_avatar.png",
    docsDir: "src",
    pageSize: 5,
    startPage: 0,
    search: false,
    smoothScroll: true,
    siteBanner: {},
  },
  plugins: [
    [
      "vuepress-plugin-rss",
      {
        base_url: "/",
        site_url: "https://boleary.dev",
        filter: (frontmatter) => frontmatter.date <= new Date(currentDateUTC),
        count: 20,
      },
    ],
    "vuepress-plugin-reading-time",
    "vuepress-plugin-janitor",
  ],
  head: [
    [
      "link",
      {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/apple-icon.png",
      },
    ],
    [
      "link",
      {
        rel: "icon",
        sizes: "32x32",
        href: "/favicon-32x32.png",
      },
    ],
    [
      "link",
      {
        rel: "icon",
        sizes: "16x16",
        href: "/favicon-16x16.png",
      },
    ],
    [
      "link",
      {
        rel: "manifest",
        href: "/site.webmanifest",
      },
    ],
    [
      "link",
      {
        rel: "mask-icon",
        href: "/safari-pinned-tab.svg",
        color: "#5bbad5",
      },
    ],
    [
      "meta",
      {
        name: "msapplication-TileColor",
        content: "#da532c",
      },
    ],
    [
      "meta",
      {
        name: "theme-color",
        content: "#ffffff",
      },
    ],
    [
      "script",
      {
        src: "/tweet.js",
      },
    ],
    [
      "script",
      {
        src: "https://olearycrew.disqus.com/count.js",
        async: true,
        id: "dsq-count-scr",
      },
    ],
    [
      "script",
      {
        src: "https://umami.boleary.dev/umami.js",
        defer: true,
        "data-website-id": "88179e35-ca51-45e5-9e22-59edf8e1d170",
      },
    ],
    [
      "meta",
      {
        name: "twitter:card",
        content: "summary",
      },
    ],
    [
      "meta",
      {
        name: "twitter:site",
        content: "@olearycrew",
      },
    ],
    [
      "meta",
      {
        name: "twitter:title",
        content: "Brendan O'Leary",
      },
    ],
    [
      "meta",
      {
        name: "twitter:description",
        content:
          "Not your typical tech speaker, my talks include interesting takes on technology's influence and power in society today",
      },
    ],
    [
      "meta",
      {
        name: "twitter:creator",
        content: "@olearycrew",
      },
    ],
    [
      "meta",
      {
        name: "twitter:image",
        content: "http://boleary.dev/img/cover.png",
      },
    ],
    [
      "meta",
      {
        name: "og:title",
        content: "Brendan O'Leary",
      },
    ],
    [
      "meta",
      {
        name: "og:type",
        content: "website",
      },
    ],
    [
      "meta",
      {
        name: "og:url",
        content: "https://boleary.dev",
      },
    ],
    [
      "meta",
      {
        name: "og:site_name",
        content: "boleary.dev",
      },
    ],
    [
      "meta",
      {
        name: "og:description",
        content:
          "Not your typical tech speaker, my talks include interesting takes on technology's influence and power in society today",
      },
    ],
    [
      "meta",
      {
        name: "og:image",
        content: "http://boleary.dev/img/cover.png",
      },
    ],
    [
      "meta",
      {
        name: "og:image:secure_url",
        content: "http://boleary.dev/img/cover.png",
      },
    ],
    [
      "meta",
      {
        name: "og:image:type",
        content: "image/png",
      },
    ],
    [
      "meta",
      {
        name: "og:image:width",
        content: "550",
      },
    ],
    [
      "meta",
      {
        name: "og:image:height",
        content: "300",
      },
    ],
    [
      "meta",
      {
        name: "og:locale",
        content: "en_US",
      },
    ],
  ],
};
