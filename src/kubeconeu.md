# KubeCon EU 2022

## Don't be shy, say Hi 😁
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/1c052cdaf61841a7adf74446d27d64f7" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Calendar
You can find my team's [calendar here](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/#-team-calendar)

## GitLab Activies

### Lightning Talks
I'm giving two Lightning talks at the GitLab booth - see the schedule below for more info:

- Consumers to Contributors
- The Era of Platforms

### Code Challenge
Participate in the Code Challenge at the GitLab Activation Zone booth! Learn more [here](https://codechallenge.dev).

### Schedule & Details
Learn more about the GitLab Activies around Kube Con [here](https://page.gitlab.com/kubeconeu-spainfy23-registration-page.html).

## Get in touch
The best way to get in touch would be to [DM me on Twitter](https://twitter.com/olearycrew).  From there we can move to Signal or WhatsApp or iMessage...I just don't want to write my phone number on a website 😉