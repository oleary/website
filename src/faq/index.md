# Frequently Asked Questions

## Ask a question
Want me to add a question here? Get in touch on [Twitter](https://twitter.com/olearycrew)

## Asked & Answered

### What could you talk about for 45 minutes with no preparation?

1. DevOps
1. The musical [The Last Five Years](https://en.wikipedia.org/wiki/The_Last_Five_Years)
1. [Margaret Hamilton](https://en.wikipedia.org/wiki/Margaret_Hamilton_(software_engineer)) & the [Apollo Guidance Computer](https://www.theatlantic.com/science/archive/2019/07/underappreciated-power-apollo-computer/594121/)
1. George Washington’s [farewell address](https://www.ourdocuments.gov/doc.php?flash=false&doc=15&page=transcript) as the most underrated document in US political history.

[Source](https://twitter.com/olearycrew/status/1398640010929356803)