---
title: My Desk
meta:
  - name: 'twitter:title'
    content: 'My Desk'
  - name: 'twitter:description'
    content: 'My desk setup as a remote developer evangelist.'
  - name: 'og:title'
    content: 'My Desk'
  - name: 'og:description'
    content: 'My desk setup as a remote developer evangelist.'
---

# My Desk

I sometimes get asked about my desk setup, and I figured it would be nice to share that easily with folks.

Ever since the pandemic, many more folks are working from home and trying to understand how to make that work. I've shared several tips for being [Suddenly Remote](/talks/2020-05-05-suddenly-remote.html) before, but I thought a deep dive into my setup would help some.

<!-- For more details on the setup see [my most recent blog post on how I do remote tech]().  -->

This page will serve as the evergreen spot for my **current** setup and tech.

## Current Setup
Currently I'm using a two desk setup - one desk as my daily work desk and one as my [streaming desk](https://brendan.fyi/twitch).  I didn't like having all the complex equipment around me for "daily driving" and so I moved to this system in early 2022.  Time will tell if this was a good idea or not.

<Desk/>

1. [Elgato Wave:1 Microphone](https://www.elgato.com/en/wave-1) - this is my "daily driver" microphone. And the Wave Link software is what I use when streaming on [Twitch](https://twitch.tv/olearycrew)
2. [Elgato Stream Deck](https://amzn.to/34PFglS) - comes in various sizes. Controls lights, camera, microphone
3. [IQUNIX F96](https://iqunix.store/collections/all) - great Bluetooth 96% mechanical keyboard with Cherry MX Brown switches
4. [DinoFire Presentation Clicker](https://amzn.to/3dusRYO) - I use this even when presenting from my desk because it is quieter than a keyboard or mouse click.
5. [Apple AirPods Pro](https://amzn.to/3lNV1R8) - daily driver for listening to video calls music while working
6. [Logitech MX Master 2S](https://amzn.to/3nRWAzF)
8. [MOTU M2 USB-C Audio Interface](https://amzn.to/3nTd6zs) - this drives the SM7B below, gives it 48v of phantom power, and connects it to my MacBook
9. [Electric Stand up Desk Frame](https://amzn.to/2H73y2x) - the desk can be a standing or sitting desk. The top I made myself from some maple hardwood...but this is the kit I used to motorize it.
10. [RODE PS 1 Microphone Boom Arm](https://amzn.to/2GYp1e9)
11. [Shure SM7B](https://amzn.to/3lJ5Ow6) - go-to microphone for podcasters, this is the one I use to sound amazing on Zoom or presentations and to...well record [my podcast](https://doyouevenart.com/). Yes, it is super expensive, but I'll tell you why I think it's worth it if you ask.
12. [Elgato Key Light](https://amzn.to/2SVB5Qe) I have two of these, both [wall mounted](https://amzn.to/3o3bULm). They are expensive, but they are amazing. Good lighting is required for good video
13. [Sony Alpha A1000](https://amzn.to/33XrK0n) - Mirrorless camera paired with a [dummy battery](https://amzn.to/33WEJiO) and an [Elgato CamLink](https://amzn.to/34Yemsk) to get professional quality video into my Macbook. I bought mine used for a great price.
14. [YIOCOE Softbox](https://amzn.to/3r8P0UJ). Diffuse light is your best friend for video.
15. The smallest [Fully Standing Desk](https://amzn.to/3u5m5CT) they sell to fit in that space well. My other desk was DIY and so Fully, while expensive, did make it easier to assemble and go.
16. Wall mounted [triangle boom arm](https://amzn.to/3r7ouLn). I was tired of the light being on a stand and taking up floor space.
17. [Elgato strip lights](https://amzn.to/3IM77FY) to help with more diffuse light.
18. Apple [Pink iMac 24"](https://amzn.to/3AFaR9o). I didn't particularly appreciate working off a laptop at my desk every day, so I bought myself this.
19. The [cutest](https://twitter.com/olearycrew/status/1481989265991024644) Apple Watch charger I've ever seen - the [elago W3 Stand](https://amzn.to/3IIgQgA)/
20. The [Lego Saturn V rocket](https://amzn.to/3AMQnf5). I heard they were discontinuing it, and so I bought it...but I don't think they did. I do not regret this.
21. A [Herman Miller Aeron](https://amzn.to/3g5H4NP) I bought brand new because I wanted size C. My credit card hated me, but my back loves me for this purchase.
22. Some old Samsung TV I had lying around in my basement...I didn't think my massive curved monitor would look good there, but I needed a screen for OBS/Twitch chat for [streaming](https://brendan.fyi/twitch).
23. Kid drawings. Estimated value: $0 or priceless. But I hang them up with these awesome [magentic strips](https://amzn.to/3u2DEDM) to easily rotate them. We have those strips in the kid's rooms too.

 
### MacBook

My MacBook isn't labeled above but is a 13" 2020 MacBook Pro.

- **Processor**: Apple M1
- **Memory**: 16 GB
- **Graphics**: Apple M1 integrated GPU
- **Hard Drive**: 500 GB

#### Apps I use

Below is a list of apps that I use on my Mac. I don't currently have affliate links to any of these, I just love them

- [1Password](https://1password.com)
- [Bartender 4](https://www.macbartender.com/Bartender4/)
- [Caffeine](https://www.intelliscapesolutions.com/apps/caffeine)
- [Obsidian.md](https://obsidian.md)
- [VSCode](https://code.visualstudio.com/download)
- [CleanShot X](https://cleanshot.com)
- [Rectangle](https://rectangleapp.com/)
- [f.lux](https://justgetflux.com/)
- [Rocket](https://matthewpalmer.net/rocket/)
- [iStats Menus](https://bjango.com/mac/istatmenus/)

### Other Items

Other items that are not pictured:

- Lots and lots of ☕
- A much more advanced [Doosl Presentation Remote with 1000ft range](https://amzn.to/350MFyU) I use in person
- [Glide Gear TMP100](https://amzn.to/3lMuLqA) iPad telepromter I use for recording formal videos. For daily driver teleprompting I use [telepromt.me](https://teleprompt.me/)
- My [USB-C hub](https://amzn.to/3nON4Nr) and [second hub](https://amzn.to/2GPQcZ7) that makes it one plug to place and remove my laptop for power, HDMI and all the USB devices
- The normal cluter of mail, random fidgets and other stuff that is on the desk
- [In ear monitors](https://amzn.to/3dxBbH5) and [extension cable](https://amzn.to/33UmLO1) - for presentations and recordings where I want a professional look. Used in conjunction with MOTU M2 Audio Interface

> **Note, in the interest of transparency:** I am not currently "sponsored" by any of these products...but for the links that are to Amazon they _are_ affiliate links where I would get a small commission if you buy them there.

#### Why is there no #7?
Look...making these pictures is hard. And coordinating it with the text on the page? Forget about it. Life is full of mistakes - don't take yourself too seriously. If you've noticed it, and I linked you to this anchor...good job.

Also, if you read this, please start a conspiracy theory about why I don't like the number 7. Somehow involve us living in a simulation or aliens or something. Thanks.
