---
title: Brendan O'Leary | Portfolio
---

# On this site
## Blog
See [my blog](https://blog.boleary.dev).

## Speaking & Talks
See [my talks](/talks/) or my [speaker bio](/talks/bio/) which as all of my media resources.

## Side Projects
Check out some of my [side projects](/side-projects/).

# At GitLab
At GitLab, I've held several roles as we've grown exponentially into the largest all-remote company in the world.  I've helped run our professional service team, been a product manager for our CI tool, and most recently became a Developer Evangelist. You can learn more about those experiences on [my resume](/resume/).

GitLab's mission is "everyone can contribute." I've always taken that very literally - and contribute well beyond my role. Even though GitLab has hired over 800 additional team-members since I started, I still find ways to contribute to GitLab and our wider community. I've contributed to [the GitLab codebase](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=brendan), our [documentation](https://gitlab.com/gitlab-org/gitlab-docs/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=brendan), all parts of our [handbook and marketing site](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&utf8=✓&state=merged&author_username=brendan), and even [infrastructure projects](https://gitlab.com/groups/gitlab-com/gl-infra/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&author_username=brendan).

## GitLab Projects
<PortfolioItem
    title="97 Things Every Cloud Engineer Should Know"
    link="https://amzn.to/3blydGa"
    description="With this book, professionals from around the world provide valuable insight into today's cloud engineering role."
    image="/img/p_97things.png"
/>

<PortfolioItem
    title="LabWork"
    link="https://labwork.dev"
    description="Lab Work is a collection of applications made to show off interesting ideas and applications I've made."
    image="/img/p_labwork.png"
/>

<PortfolioItem
    title="#Git15"
    link="https://git15.labwork.dev"
    description="For the 15th anniversary of git in 2020 - a collection of resources to help give the history of git, how it works and where it's going in the next 15 years."
    image="/img/p_git15.png"
/>

<PortfolioItem
    title="GitLab All the Things"
    link="https://gitlab.com/allthethings"
    description="GitLab All the Things hopes to show examples of how to use GitLab to build and deploy all types of code to all kinds of deployment environments."
    image="/img/allthethings.png"
/>

<PortfolioItem
    title="GitLab Theme"
    link="https://gitlabtheme.com/"
    description="A theme for a bunch of platforms, all inspired by GitLab's branding."
    image="/img/p_gitlabtheme.png"
    :extralinks="[
        {text: 'Checkout the repository', link: 'https://gitlab.com/gitlabtheme/'},
    ]"
/>

## Videos
At GitLab, everything is public by default. I've been able to create several videos about how to use GitLab, how we work together, and what we plan to do in the future. 

* [GitLab vs Jenkins, where is GitLab _weaker_?](https://www.youtube.com/watch?v=3Wr3O6mY5VE)
* [Kubernetes 101](https://www.youtube.com/watch?v=rq4GZ_GybN8&t=674s)
* [Migrating from Jenkins to GitLab](https://www.youtube.com/watch?v=RlEVGOpYF5Y)
* [Migrating from GitHub to GitLab](https://www.youtube.com/watch?v=VYOXuOg9tQI)
* [Git-ing started with Git](https://www.youtube.com/watch?v=Ce5nz5n41z4)
* [The Cube: Discussing multi- and hybrid-cloud](https://www.youtube.com/watch?v=-eS2_raHa6Y&feature=emb_title)
* [GitLab Flow](https://www.youtube.com/watch?v=InKNIvky2KE)
* [GitLab Namespaces (users, groups and subgroups)](https://www.youtube.com/watch?v=r0sJgjR2f5A)
* [Three keys to making the right multicloud decisions](https://www.youtube.com/watch?v=Qwins6ZScCo)
