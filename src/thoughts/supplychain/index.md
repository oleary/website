---
title: 'Software Supply Chain Security'
date: 2020-11-08 05:00:00
meta:
  - name: 'twitter:title'
    content: 'Software Supply Chain Security'
  - name: 'twitter:description'
    content: "Software supply chain attacks are not a new attack vector - but one that we've seen increasing use of as many prominent organizations have created sophisticated security practices internally. When this happens, attackers look for other weaknesses - and more often than not, those weak points are often in the supply chains those organizations use."
  - name: 'og:title'
    content: 'Software Supply Chain Security'
  - name: 'og:description'
    content: "Software supply chain attacks are not a new attack vector - but one that we've seen increasing use of as many prominent organizations have created sophisticated security practices internally. When this happens, attackers look for other weaknesses - and more often than not, those weak points are often in the supply chains those organizations use."
---

# Software Supply Chain Security

## About

Software supply chain attacks are not a new attack vector - but one that we've seen increasing use of as many prominent organizations have created sophisticated security practices internally. When this happens, attackers look for other weaknesses - and more often than not, those weak points are often in the supply chains those organizations use.

On this page, I want to collect articles and details about recent and novel supply chain style attacks, all of which help inform a [talk on supply chain security](https://cfps.dev/u/brendan/309638467550184004) that I'm constantly revising.

## On this page

- [What is a Software Supply Chain?](#what-is-a-software-supply-chain)
- [Futher Reading](#futher-reading)
  - [From me 😃](#me)
  - [From others](#other-people)
- [Software Bill of Materials](#software-bill-of-materials)
- [List of Supply Chain Attacks](#supply-chain-attacks)
  - [Detailed list from the CNCF](https://github.com/cncf/tag-security/tree/master/supply-chain-security/compromises)

## What is a Software Supply Chain?

The supply chain is much broader than many give it credit for. When I say "software supply chain," what's the first thing that comes to mind? Often that would the direct dependencies of the code itself - open source dependencies brought into the code. And while open-source dependencies are an essential part of the software supply chain, they are far from the _only_ point to consider when thinking about supply chain security.

Other parts of your supply chain include the code you write, any 3rd party libraries you're using (not just the open sources ones), the dependencies you inherit by proxy (the dependencies of your dependencies), your DevOps tools and processers. Any tools and plugins those tools use, any vendor code, and any dependencies that those vendors have.

## Futher Reading

### Me

- [My talk on Supply Chain security](https://cfps.dev/u/brendan/309638467550184004 that I've given a number of iterations of, including:
  - [FOSDEM 2021](https://ftp.osuosl.org/pub/fosdem/2021/D.dependency/dep_as_strong_as_the_weakest_link.webm)
  - [GDIT Emerge](https://www.gdit.com/perspectives/emerge/)
  - [DockerCon 2021](https://www.docker.com/dockercon-live/2021/content/Videos/EXiQKsKGfgrMXWTT3).

### Other People

- Johnathan Hunt, VP of Security at GitLab [3 best practices for better supply chain security](https://techbeacon.com/security/3-best-practices-better-supply-chain-security)
- Read Cindy Blake's [article on how your DevOps platform impacts supply chain security](https://about.gitlab.com/blog/2021/04/28/devops-platform-supply-chain-attacks/).

## Software Bill of Materials (SBOM)

- [What is an SBOM, and why should you Care??](https://boxboat.com/2021/05/12/what-is-sbom-and-why-should-you-care/)
- US National Telecommunications and Information Administration's resources on [Software Bill of Materials](https://www.ntia.gov/SBOM)

## Supply Chain Attacks

Below are some supply chain attacks I've done some amount of research on in the past. You can also find a complete open list of supply chain attacks and breaches on the [CNCF TAG Security repo](https://github.com/cncf/tag-security/tree/master/supply-chain-security/compromises).

### Solarwinds/Sunburst

Attacking the DevOps environment - in this case the CI/CD systems which are very poorly secured - allows attackers to inject and sign malicious software.

- [U.S. Treasury, Commerce Depts. Hacked Through SolarWinds Compromise](https://krebsonsecurity.com/2020/12/u-s-treasury-commerce-depts-hacked-through-solarwinds-compromise/)
- [Highly Evasive Attacker Leverages SolarWinds Supply Chain to Compromise Multiple Global Victims With SUNBURST Backdoor](https://www.fireeye.com/blog/threat-research/2020/12/evasive-attacker-leverages-solarwinds-supply-chain-compromises-with-sunburst-backdoor.html)

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Ding ding ding. The CI/CD box is always:<br><br>* Manually configured<br>* Able to touch production<br>* Accessed by a whole mess of people<br>* Scary to patch<br><br>More on this after the insurrection. <a href="https://t.co/YRjPx1WMqH">https://t.co/YRjPx1WMqH</a></p>&mdash; Corey Quinn (@QuinnyPig) <a href="https://twitter.com/QuinnyPig/status/1346950564869902337?ref_src=twsrc%5Etfw">January 6, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Birsan Dependency Bug Bounties

Hacking Apple, PayPal, Shopify, Netflix, Yelp, Tesla, and Uber with open source dependancy man-in-the-middle style attacks.

- [Dependency Confusion: How I Hacked Into Apple, Microsoft and Dozens of Other Companies](https://medium.com/@alex.birsan/dependency-confusion-4a5d60fec610)
- [Researcher hacks over 35 tech firms in novel supply chain attack](https://www.bleepingcomputer.com/news/security/researcher-hacks-over-35-tech-firms-in-novel-supply-chain-attack/)

### PHP source code attack

Inserting backdoors into 78% of the internet by attacking the source code management system.

- [PHP's Git server hacked to add backdoors to PHP source code](https://www.bleepingcomputer.com/news/security/phps-git-server-hacked-to-add-backdoors-to-php-source-code/)

### Network-devices as a vector to homes & businesses

- [Whistleblower: Ubiquiti Breach “Catastrophic”](https://krebsonsecurity.com/2021/03/whistleblower-ubiquiti-breach-catastrophic/)

### Unintentional supply chain disruptions

#### Undiscovered open source vulnerabilities

- Node.js: [npm/netmask: undiscovered for 9 years](https://portswigger.net/daily-swig/ssrf-vulnerability-in-npm-package-netmask-impacts-up-to-279k-projects)

#### Pulling popular open source libraries unexpectedly

- Ruby on Rails: [mimemagic gem license issue](https://github.com/rails/rails/issues/41750)
- Node.js: [leftpad](https://qz.com/646467/how-one-programmer-broke-the-internet-by-deleting-a-tiny-piece-of-code/)

## Comments

<CommentoPageView/>
