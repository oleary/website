---
title: "Consumers to Contributors"
date: 2022-06-14 05:00:00
meta:
  - name: "twitter:title"
    content: "Consumers to Contributors"
  - name: "twitter:description"
    content: "Open source software is eating the world. The new winners and losers in every industry will be determined by those who are able to best leverage it - to move from simple consumers of open source to customers and contributors of open source. Investments now can pay dividends for decades to come."
  - name: "og:title"
    content: "Consumers to Contributors"
  - name: "og:description"
    content: "Open source software is eating the world. The new winners and losers in every industry will be determined by those who are able to best leverage it - to move from simple consumers of open source to customers and contributors of open source. Investments now can pay dividends for decades to come."
---

# Consumers to Contributors

## About

Open source software is eating the world. The new winners and losers in every industry will be determined by those who are able to best leverage it - to move from simple consumers of open source to customers and contributors of open source. Investments now can pay dividends for decades to come.

On this page, I want to collect articles and details about the business case for open source, all of which help inform a [talk on moving from consumers to contributors](https://cfps.dev/u/brendan/326120625945969232) that I'm constantly revising.

## On this page

- [Why is Open Source Important?](#why-is-open-source-important)
- [Futher Reading](#futher-reading)

## Why is Open Source Important?

As firms are talking more and more about [supply chain security](/thoughts/supplychain/), it's essential to take a step back and understand that supply chain at a higher level. The supply chain for the vast majority of modern software applications is heavily sprinkled with open source software. 

As we know from history, those who can best understand and master their supply chain are poised to take advantage of it in their respective markets. Understanding the "raw materials" of software as the underpinning open source software allows organizations to better utilize that software and better align their supply chain to their organizational goals.

## Further Reading

- [Unlock Open](https://unlockopen.com/)
- [The Hidden Benefit of Giving Back to Open Source Software](https://hbswk.hbs.edu/item/the-hidden-benefit-of-giving-back-to-open-source-software) from HBS
- [Why Do User Communities Matter for Strategy?](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3407610) from HBS
- [Learning by Contributing: Gaining Competitive Advantage Through Contribution to Crowdsourced Public Goods](https://pubsonline.informs.org/doi/10.1287/orsc.2018.1202)
- [Open Source Case for Business](https://opensource.org/advocacy/case_for_business.php)

## Comments

<CommentoPageView/>
