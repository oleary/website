---
title: Speaker Rider
---

# Speaker Rider
> Last Updates: 2021-11-10

So you want me to speak at your event - that's awesome!  I'm honestly very flattered, and if you're reading this, I'm probably excited to speak at the event too! 

Before I do, I want to make sure you understand that I want to make sure that the conference aligns with my values around making sure that everyone can contribute.  And that means having both good intentions and a conference that accurately reflects those intentions.

## My Response
If you reach out to me with a speaking request and I might be available, I'll respond with this:

> Thanks for your interest! A few questions first: do you have an enforced, public Code of Conduct? How do you approach the diversity of your speaker lineups? How do you value inclusion at your event?  For more details, you can read the specifics of my rider here: https://boleary.dev/rider.

If you're able to articulate that clearly, we're probably in a good place.  Otherwise, I may ask you to clarify specific points related to the rest of this rider.

## Rider

### 🎤 In-Person Events
- My employer may cover my travel costs if I am representing them on stage. 
- I expect you to cover these costs for any speaker whose employer is not covering the expenses: airfare, lodging for the length of the conference (+/- 1 day for international travelers), local transportation to and from the event and airport.
- Ensure you can either reimburse speakers OR purchase travel and accommodations on behalf of the speaker as desired by the speaker. (e.g., I'd rather book and be reimbursed, but some may not be able to cover that kind of cost ahead of reimbursement).  This includes the ability to have a conference credit card on file for incidentals at the hotel
- Speakers should be given a ticket for the entire event...you want your speakers to participate in the rest of the event
- Consider if you can budget for student or underrepresented group scholarships to attend (travel, ticket cost, etc.)

#### Logistics
- Wireless microphone option (handheld or lavalier)
- Microphone for audience members if you plan to do Q&A
- Confidence monitor visible by the speaker, as a "second monitor" for my slides and speaker notes.  Depending on the venue, this can often be my laptop screen.

### 📹 Remote Events
- For pre-recorded events, please consider the extra time required to record a presentation.  I also recommend you have a professional video editor. That way I can provide you a 3 video version (my face, my slides, my face and slides) from Zoom and you can edit it nicely.
- For live events, please make sure that you have a clear plan and run of show for any issues with internet instability. 

#### Logistics 
- Code of Conduct is still a requirement, and I expect moderators and staff from the conference in all places where attendees can gather (chat, etc.)
- The event platform should be accessible to WCAG AA standards.

### 💰 Speaker Honorarium
- If there are multiple speakers, they must all be paid equally
- For a for-profit conference, I'd expect an honorarium for most speakers - you should value their time.
- Do not expect speakers to accept "exposure" as a substitute for being paid for their work.

### 🎟️  Event Logistics
#### Code of Conduct
A Code of Conduct and enforcement are requirements.  Yes, it can feel like insurance - you don't need it until you do, so it can be "easy" to forget.  In the same way, you might be tempted to say "Don't be a jerk" and move on...but would you be happy with your home insurance contract saying: "If your house burns down, we promise to not be jerks"?

- Have a public code of conduct that everyone attending (speakers, vendors, attendees) are bound to abide by
- Have clear, easy reporting methods for any violations
- See [JSConfEU](https://2019.jsconf.eu/code-of-conduct/) or the [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/) for clear examples.

#### No white-majority, all-men lineups
- Everyone at the conference speaking should not look like me (white male)
- The conference should make a serious effort to recruit underrepresented minorities to speak, and the final speaker lineup should reflect that effort
- Underrepresented minorities should not be invited exclusively to speak about their experiences as a minority in tech. 
- Any panel that I'm on must include at least one person who does not identify as a man

#### Accessibility and Inclusion
- I expect all spaces used for and by the conference (including venues for speaker's dinners, social events, etc.) to be accessible.  You can visit ada.gov for any questions
- Captioning: For live events, ideally by a human live-captioning company.  If ticket sales support your event, you should work this into the cost of the event.  For pre-recorded events, captions must be captioned by a human
- Provide attendees the ability (stickers, writing space, pins) to identify the pronouns they use.
- Ensure that the MC/host gets name pronunciation and pronoun details from anyone they are introducing directly

### License of this rider
This license was inspired by some amazing folks like [Cassidy Williams](https://github.com/cassidoo/talks/blob/master/speaker-rider.md), [Tatiana Mac](https://gist.github.com/tatianamac/493ca668ee7f7c07a5b282f6d9132552), [Matt Stratton](https://gist.github.com/mattstratton/da8c314561010dff50ee1effc899772b), and [Kat Cosgrove](https://twitter.com/Dixie3Flatline).  You can reuse this rider for yourself, as the text of this rider is released to the public domain under the [CC0 License](https://creativecommons.org/share-your-work/public-domain/cc0/)
