---
title: Suddenly Remote
talk: true
tagline: Find yourself suddenly working where you also live? How do you make your life and work well...work...in such uncertain times?
abstract: |
    For all of those grappling with the new reality of remote working, the challenges and opportunities are plentiful. Even for those of us who were working remotely before the current crisis, we have had to adjust our lives in ways we never anticipated. How do you make your life and work well...work...in such uncertain times?

    In this talk, we'll discuss the tried and true strategies we've discovered at GitLab while building the largest all-remote company in the world. It is important to separate work from life, find a structure that works for you and your loved ones, and allow yourself to try new things quickly. Also, social distance doesn't have to mean losing the social aspect of life.

    By being intentional about the way you address being remote - either suddenly or for your career - you can avoid burnout and loneliness while finding new ways to leverage your time and be more effective than when you used to go to the office every day.

# Originally based on https://drive.google.com/file/d/1aB5NrAck8GdyyO3Q8LzllUTJwSZFaFB8/view?ths=true 
#slides: https://docs.google.com/presentation/
#video: https://www.youtube.com/
#events:
#  - name: A Great Event
#    date: "2020-02-02"
#    href: https://www.youtube.com/
---

<Talk :talk="$page.frontmatter"/>
