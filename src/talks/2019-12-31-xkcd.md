---
title: "All I need to know about DevOps I learned from XKCD"
talk: true
tagline: XKCD comics are more than just funny, you can really learn great DevOps lessions in these clever dorky comics.
abstract: |
    XKCD describes itself as “a webcomic of romance, sarcasm, math, and language.” What if it is more? What if XKCD and it’s creator Randall Munroe have slowly been revealing what software development, DevOps and team collaboration are all about.

    In this talk, we’ll take a look at some recurring themes in XKCD comics - and how they hit home with recurring themes in DevOps. From regular expressions, vim vs. emacs to user experiences so bad...they are literally a joke XKCD finds ways to express simply the thoughts we’ve all had. We will examine these comics for their deeper meaning (deeper even than just the tooltip text Randall leaves behind) and learn from them how to make better software. 
slides: https://docs.google.com/presentation/d/e/2PACX-1vTz96G8KN0VMFJJFl6ww9AZwoH57m9HirSiSHqRKW3zoWqMtd6UXuvBqD-IKul5giGWzR4k86R3Wmll/embed
#video: https://www.youtube.com/
#events:
#  - name: A Great Event
#    date: "2020-02-02"
#    href: https://www.youtube.com/
---

<Talk :talk="$page.frontmatter"/>
